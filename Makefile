#
# makefile for `git-debubble'
#
VERS=$(shell sed <git-debubble -n -e '/version *= *"\(.*\)"/s//\1/p')

SOURCES = README.adoc COPYING NEWS.adoc git-debubble git-debubble.adoc Makefile \
	control git-debubble-logo.png

all: git-debubble.1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f *~ *.1 *.html rm -f MANIFEST

install: git-debubble.1 uninstall
	install -m 0755 -d $(DESTDIR)/usr/bin
	install -m 0755 -d $(DESTDIR)/usr/share/man/man1
	install -m 0755 git-debubble $(DESTDIR)/usr/bin/
	install -m 0644 git-debubble.1 $(DESTDIR)/usr/share/man/man1/

uninstall:
	rm -f /usr/bin/git-debubble /usr/share/man/man1/git-debubble.1

version:
	@echo $(VERS)

git-debubble-$(VERS).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:git-debubble-$(VERS)/: >MANIFEST
	@(cd ..; ln -s git-debubble git-debubble-$(VERS))
	(cd ..; tar -czf git-debubble/git-debubble-$(VERS).tar.gz `cat git-debubble/MANIFEST`)
	@ls -l git-debubble-$(VERS).tar.gz
	@(cd ..; rm git-debubble-$(VERS))

dist: git-debubble-$(VERS).tar.gz

release: git-debubble-$(VERS).tar.gz git-debubble.html
	shipper version=$(VERS) | sh -e -x

refresh: git-debubble.html
	shipper -N -w version=$(VERS) | sh -e -x
